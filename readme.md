# Employee Service

## Objectives

* Implement previously discussed architecture alternative #1
* Deliver sample user-interface and end-point
* Leverage Angular for Single Page Application front-end
* Use serverless [consumption plan](https://docs.microsoft.com/en-us/azure/azure-functions/functions-scale#consumption-plan) to host dotnet core end-point
* Retrieve employees data from Azure Active Directory in read-only fashion

## Overview

![](./assets/overview.jpg)

## User-Interface

![](./assets/ui.jpg)

* User-Interface assets hosted on Azure Blob
* Angular User Interface: https://sakpgospocemployeeng.z20.web.core.windows.net/
* ReactJS User Interface: https://sakpgospocemployeerj.z20.web.core.windows.net/
* Dotnet Blazor WebAssembly user interface: https://sakpgospocemployeebz.z20.web.core.windows.net/

## End-Point

![](./assets/end-point.jpg)

* End-point deployed as Serverless function using [consumption plan](https://docs.microsoft.com/en-us/azure/azure-functions/functions-scale#consumption-plan) to reduce hosting costs
* End-point specs: https://app.swaggerhub.com/apis/vkhazin/employee-svc/1.0.0
* The end-point exposes data in a read-only fashion with a limited functionality, it is not an AD management API
* End-point deployment: https://kpgos-poc-employee-svc.azurewebsites.net/employees

## Authentication

* No authentication is required to access Angular Application from a web browser
* No authentication is required to access the end-point whether from a browser or directly
* Azure AD application token will be used to leverage graph API

## DevOps

* Azure DevOps will host the repositories
* Azure DevOps will be used to demonstrate the no-infrastructure CI/CD pipeline